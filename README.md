# Settings.
========================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist exoodev/yii2-settings
```

or add

```json
"exoodev/yii2-settings": "*"
```

to the require section of your composer.json.



Usage
-----

Example configuration

```php
'settings' => [
    'class' => 'exoo\settings\components\Settings',
],
```

Example configuration for advanced template

```php
'settings' => [
    'class' => 'exoo\settings\components\Settings',
    'cache' => [
        'class' => 'yii\caching\FileCache',
        'cachePath' => '@backend/runtime/cache',
    ]
],
```

## Migration

```
yii migrate --migrationPath=@exoo/settings/migrations
```
