<?php

namespace exoo\settings;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
	/**
     * @inheritdoc
     */
	public function bootstrap($app)
	{
		$app->i18n->translations['settings'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@exoo/settings/messages',
		];
	}
}
