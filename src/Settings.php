<?php

namespace exoo\settings;

use Yii;
use yii\helpers\Json;
use yii\caching\Cache;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\di\Instance;

/**
 * Settings
 *
 * For example
 *
 * ```php
 * Yii::$app->settings->get('blog'); // all settings
 * Yii::$app->settings->get('blog', 'postsPerPage');
 * Yii::$app->settings->set('blog', 'postsPerPage', 10);
 * ```
 *
 */
class Settings extends Component
{
    /**
     * @var Cache|array|string the Cache object or the application component ID of the user component.
     */
    public $cache = 'cache';
    /**
     * @var string the cache key.
     */
    public $cacheKey = 'settings';
    /**
     * @var string the table name setting.
     */
    public $tableName = '{{%settings}}';
    /**
     * @var array array list of settings in key-value pairs format.
     */
    protected $_data = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->cache = Instance::ensure($this->cache, Cache::className());
        $this->initSettings();
    }

    /**
     * Get values.
     * @param string $group the group name.
     * @param string $key the setting name.
     * @return mixed
     */
    public function get($group, $key = null, $default = null)
    {
        $params = [];

        if (isset(Yii::$app->params[$group])) {
            $params = Yii::$app->params[$group];
        }
        if (isset($this->_data[$group])) {
            $params = array_merge($this->_data[$group], $params);
        }
        if (empty($params)) {
            return $default;
        }

        foreach ($params as $index => $param) {
            if ($this->isJson($param)) {
                $params[$index] = Json::decode($param);
            }
        }

        if (!empty($key)) {
            $value = ArrayHelper::getValue($params, $key);
            return $value === null ? $default : $value;
        }

        return $params;
    }

    /**
     * Remove any elements where the value is empty
    * @param  array $array the array to walk
    * @return array
    */
    function removeEmptyValues(array &$array) {
        foreach ($array as $key => &$value) {
            if (is_array($value)) {
                $value = $this->removeEmptyValues($value);
            }
            if (empty(array_filter($array))) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    /**
     * Saves settings. If parameter already exists the data will be updated, else the data will be inserted.
     *
     * @param string $key parameter name.
     * @param mixed $value parameter value.
     * @return boolean
     * @throws [[yii\db\Exception]] execution failed.
     */
    public function set($group, $key, $value, $type = null)
    {
        if (is_array($value)) {
            $value = Json::encode(array_filter($this->removeEmptyValues($value)));
        }

        $exists = ArrayHelper::getValue($this->get($group), $key);
        $connection = Yii::$app->db;

        if ($exists !== null) {
            $connection->createCommand()
                ->update($this->tableName, ['value' => $value], ['group' => $group, 'key' => $key])
                ->execute();
        } else {
            $connection->createCommand()
                ->insert($this->tableName, [
                    'group' => $group,
                    'key' => $key,
                    'value' => $value,
                    'type' => $type !== null ? $type : gettype($value),
                ])
                ->execute();
        }

        return $this->cacheDelete();
    }

    /**
     * Deletes cache.
     * @return boolean If no error happens during deletion.
     */
    public function cacheDelete()
    {
        return $this->cache->delete($this->cacheKey);
    }

    /**
     * Inits settings.
     */
    protected function initSettings()
    {
        if (!$settings = $this->cache->get($this->cacheKey)) {
            $settings = Yii::$app->db->createCommand('SELECT * FROM ' . $this->tableName)->queryAll();
            $this->cache->set($this->cacheKey, $settings);
        }

        foreach ($settings as $setting) {
            if ($setting['type']) {
                settype($setting['value'], $setting['type']);
            }

            $this->_data[$setting['group']][$setting['key']] = $setting['value'];
        }
    }

    protected function isJson($value) {
        return is_string($value) && is_array(json_decode($value, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}
