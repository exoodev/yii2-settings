<?php

namespace exoo\settings\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;

/**
 * SettingsAction class
 */
class SettingsAction extends Action
{
    /**
     * @var string class name of the model which will be handled by this action.
     * The model class must implement [[ActiveRecordInterface]].
     * This property must be set.
     */
    public $modelClass;
    /**
     * @var string the prefix name (default value - module id)
     */
    public $group;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if ($this->modelClass === null) {
            throw new InvalidConfigException(get_class($this) . '::$modelClass must be set.');
        }

        if ($this->group === null) {
            $this->group = $this->controller->module->id;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $model = Yii::createObject($this->modelClass);

        if ($model->load(Yii::$app->request->post()) && $this->save($model)) {
            $message = Yii::t('settings', 'Saved');

            if (!Yii::$app->request->isPjax) {
                if (Yii::$app->request->isAjax) {
                    return $this->controller->asJson([
                        'notification' => $message,
                        'status' => 'success'
                    ]);
                }

                Yii::$app->session->setFlash('notify.success', $message);
                return $this->controller->refresh();
            }
        }

        $this->setAttributeValues($model);
        return $this->controller->render($this->id, [
            'model' => $model,
        ]);
    }

    /**
     * Add old values in model
     * @param Model $model
     */
    public function setAttributeValues($model)
    {
        foreach ($model->attributes as $attribute => $defaultValue) {
            $value = Yii::$app->settings->get($this->group, $attribute, $defaultValue);
            $model->{$attribute} = $value;
        }
    }

    /**
     * Save settings
     * @param Model $model
     * @return boolean the result saved
     */
    protected function save($model)
    {
        if (!$model->validate()) {
            return false;
        }

        if (method_exists($model, 'types')) {
            $types = $model->types();
        }

        foreach ($model->attributes as $key => $value) {
            $type = isset($types[$key]) ? $types[$key] : '';

            if ($type == 'array') {
                if (!is_array($value)) {
                    $value = explode(PHP_EOL, $value);
                }
            }

            Yii::$app->settings->set($this->group, $key, $value, $type);
        }

        return true;
    }
}
