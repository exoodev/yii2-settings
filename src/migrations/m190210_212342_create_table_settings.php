<?php

use yii\db\Migration;

class m190210_212342_create_table_settings extends Migration
{
    public function up()
    {
        $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }

        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'group' => $this->string()->notNull(),
            'key' => $this->string()->notNull(),
            'value' => $this->text()->notNull(),
            'type' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('app', '{{%settings}}', ['group', 'key'], true);
    }

    public function down()
    {
        $this->dropTable('{{%settings}}');
    }
}
